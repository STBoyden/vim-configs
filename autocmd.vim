" AUTOCOMMANDS
if has("autocmd")
  augroup filetypedetect
    au BufEnter *.markdown,*.mkd,*.md,*.rmd setl wrap tw=79
    au BufEnter *.json setl ft=javascript
    au BufEnter *.coffee setl sw=2 expandtab
    au BufEnter *.py setl ts=4 sw=4 sts=4
    au BufEnter *.php setl ts=4 sw=4 sts=4
    au BufEnter *.js setl ts=2 sw=2 sts=2
    au BufEnter *.html setl ts=4 sw=4 sts=4
    au BufEnter *.tex setl wrap tw=79 fo=tcqor
    au BufEnter *.[ch] setl cindent
    au BufEnter *.[ch]pp setl cindent
    au BufEnter Makefile setl ts=4 sts=4 sw=4 noet list
    au BufEnter *.es6 setf javascript
  augroup END

  " when enabling diff for a buffer it should be disabled when the
  " buffer is not visible anymore
  au BufHidden * if &diff == 1 | diffoff | setlocal nowrap | endif

  " Instead of reverting the cursor to the last position in the buffer, we
  " set it to the first line when editing a git commit message
  au FileType gitcommit au! BufEnter COMMIT_EDITMSG call setpos('.', [0, 1, 1, 0])

  " Automatically removing all trailing whitespace
  autocmd BufWritePre * :call StripTrailingWhitespace()

  " Automatically source vimrc on save.
  autocmd! bufwritepost $MYVIMRC source $MYVIMRC

  " Save on FocusLost
  au FocusLost * :silent! wall " Save on FocusLost
  au FocusLost * call feedkeys("\<C-\>\<C-n>") " Return to normal mode on FocustLost

  " Disable paste mode when leaving Insert Mode
  au InsertLeave * set nopaste

  " Resize splits when the window is resized
  au VimResized * exe "normal! \<c-w>="

  " preceding line best in a plugin but here for now.
  au BufNewFile,BufRead *.coffee set filetype=coffee

  " Workaround vim-commentary for Haskell
  au FileType haskell setlocal commentstring=--\ %s
  " Workaround broken colour highlighting in Haskell
  au FileType haskell setlocal nospell
endif

" Markdown configuration
autocmd FileType markdown,rmd,txt,rtf set spell!
autocmd FileType markdown,rmd setl updatetime=1

" automagically start NERDTree on vim startup
"autocmd vimenter * NERDTree | wincmd p

" when entering a new buffer, create another instance of NERDtree if there isn't
" one (the if statement stops NERDtree from recursively opening)

"let blacklist = ['md', 'rmd', 'rtf', 'txt']
"let type = expand('%:e')
"autocmd BufWritePre * if index(blacklist, type) > 0 && !exists("g:notNERD") | unlet g:notNERD | endif

autocmd BufEnter *.[ch],*.[ch]pp,*.cs,*.py,*.html,*.xhtml if (bufwinnr('NERDTree') == -1) | NERDTree | wincmd p | endif

" open NERDTree even with no file specified
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | wincmd p | endif

" close vim if NERDTree is the only window left open
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

"autocmd FileType [ch],[ch]pp set wrap linebreak nolist showbreak=<tab>
