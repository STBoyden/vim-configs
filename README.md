## Prerequisites

- Git (1.7+)
- Vim (7.3+)
- ctags

# Installation

To install:

`mkdir ~/.vim`

`cd ~ && git clone https://bitbucket.org/STBoyden/vim-configs/src/master/`

`cd vim-configs`

`cp * ~/.vim/`

`ln -s ~/.vimrc ~/.vim/vimrc`

`cd .. && rm -rf vim-configs/`

**Done!**
