"KEYMAPPINGS
"DISABLED DEFAULT MAPPING: UNSET SHORTCUTS {{{
" Unmapping help from F1 and Ctrl-F1 for use toggling the reference manual
" the :h topic feature works, and <leader><F1> displays quickref
inoremap <F1> <nop>
nnoremap <F1> <nop>
vnoremap <F1> <nop>
"unmap the suspend function
map <C-z> <Nop>
"}}}

" Set mapleader
let g:mapleader=","

" Toggle paste mode
nmap <silent> <F4> :set invpaste<CR>:set paste?<CR>
imap <silent> <F4> <ESC>:set invpaste<CR>:set paste?<CR>

" spacebar create/open/close folding
nmap <silent> <Space> za
vmap <silent> <Space> zf

" enable/disable list
nmap <silent> <C-l> :set nolist!<CR>

" Map escape key to jj or <leader>e
imap jj <ESC>

"" Fast saving
nmap <leader>w :w!<cr>

"" Fast saving and quitting"
nmap <leader>q :wq!!<cr>

" :W sudo saves the file
" (useful for handling the permission-denied error)
"command W w !sudo tee % > /dev/null

" Move visual block
vnoremap <c-J> :m '>+1<CR>gv=gv
vnoremap <c-K> :m '<-2<CR>gv=gv

" Spell commands
autocmd FileType markdown,rmd,txt,rtf nnoremap ;ll :set<space>spell!<Enter>
nmap ?n ]s
nmap ?p [s
nmap ?+ zg
nmap ?? z=

" Make Y consistent with C and D
nnoremap Y y$

" jump to start/end of line
noremap H ^
noremap L $

" easier formatting of paragraphs
vmap Q gq
nmap Q gqap

" Keep search pattern at the center of the screen
nmap <silent> n nzz
nmap <silent> N Nzz
nmap <silent> * *zz
nmap <silent> # #zz
nmap <silent> g* g*zz
nmap <silent> g# g#zz

" Circular windows navigation
nmap <C-j> <c-w>w
nmap <C-k> <c-w>W

nnoremap <silent> <C-w>1 :only<CR>
nnoremap <silent> <C-w>2 :only<CR> <C-w>v
nnoremap <silent> <C-w>3 :only<CR> <C-w>v<C-w>s
nnoremap <silent> <C-w>4 :only<CR> <C-w>v<C-w>s<C-w>h<C-w>s

" switch to the directory of the open buffer
map <leader>cd :cd %:p:h<cr>

" set text wrapping toggles
nmap <silent> tw :set invwrap<cr>:set wrap?<cr>

" Underline the current line with '-'
nmap <silent> <leader>ul :t.<CR>Vr-

function! s:goyo_enter()
  silent !i3-msg fullscreen enable
  Limelight
  set textwidth=80
  let &sbr = nr2char(8618).' '
endfunction

function! s:goyo_leave()
  silent !i3-msg fullscreen disable
  Limelight!
endfunction

" Goyo and Limelight bindings
autocmd! User GoyoEnter nested call <SID>goyo_enter()
autocmd! User GoyoLeave nested call <SID>goyo_leave()
nnoremap <F10> :Goyo<enter>
inoremap <F10> :Goyo<enter>

" Markdown bindings
autocmd FileType markdown,rmd inoremap ;1 #<space>
autocmd FileType markdown,rmd inoremap ;2 ##<space>
autocmd FileType markdown,rmd inoremap ;3 ###<space>
autocmd FileType markdown,rmd inoremap ;4 ####<space>
autocmd FileType markdown,rmd inoremap ;5 #####<space>
autocmd FileType markdown,rmd inoremap ;6 ######<space>
autocmd FileType markdown,rmd inoremap ;j <div<space>align="justify"><enter><enter></div>
autocmd FileType markdown,rmd inoremap ;l <div<space>align="left"><enter><enter></div>
autocmd FileType markdown,rmd inoremap ;r <div<space>align="right"><enter><enter></div>
autocmd FileType markdown,rmd inoremap ;i **<Esc>i
autocmd FileType markdown,rmd inoremap ;b ****<Esc>hi
autocmd FileType markdown,rmd inoremap ;li +<space>
autocmd FileType markdown,rmd inoremap ;ld -<space>
autocmd FileType markdown,rmd inoremap ;hl <Enter>---<Enter>
autocmd FileType markdown,rmd inoremap ;co ```{}<enter><enter>```
autocmd FileType rmd          inoremap ;eq $$<Esc>i
autocmd FileType rmd          inoremap ;EQ $$$$<Esc>hi
autocmd FileType rmd          inoremap ;c @
autocmd FileType rmd          inoremap ;C [@]<Esc>i

" Compiling for different filetypes
autocmd FileType python nnoremap <c-F5> :!clear<space>&&<space>python<space>main.py<Enter>
autocmd FileType python nnoremap <F5> :!clear<space>&&<space>python<space>"%"<Enter>
autocmd FileType cpp nnoremap <F5> :!clear<space>&&<space>bash<space>~/Scripts/compile-cxx<Enter>
autocmd FileType bash,sh nnoremap <F5> :!clear<space>&&<space>./%<Enter>
autocmd FileType tex nnoremap <F5> :!clear<space>&&<space>lualatex<space>"%"<Enter><Esc>
autocmd FileType cs nnoremap <F5> :!clear<space>&&<space>mcs<space>*.cs<space>-out:"../bin/Program.exe"&&<space>mono<space>"../bin/Program.exe"<Enter>
autocmd FileType javascript nnoremap <F5> :!clear<space>&&<space>node<space><c-r>%<Enter>
autocmd FileType xdefaults nnoremap <F5> :!clear<space>&&<space>xrdb<space>merge<space>%<Enter><Esc>
autocmd FileType markdown nnoremap <F5> :!clear<space>&&<space>pandoc<space>"%"<space>-o<space>"%:r.pdf"<space>&>2<Enter><Esc>
autocmd FileType rmd nnoremap <F5> :!clear<space>&&<space>echo<space>"rmarkdown::render('<c-r>%')"<space>\|<space>R<space>--vanilla<Enter><Enter>

" preview
autocmd FileType tex nnoremap <F6> :LLPStartPreview<Enter>
autocmd FileType markdown,rmd nnoremap <F6> :!"$PDF_VIEWER"<space>"%:r.pdf"<space>\><space>/dev/null<space>&<Enter><Esc>

" remove SyntasticCheck window
nnoremap <F11> :SyntasticCheck(false)<Enter>

" For local replace
"nnoremap <leader>rl gd[{V%::s/<C-R>///gc<left><left><left>

" For global replace
nnoremap <F4> :%s/\(<c-r>=expand("<cword>")<cr>\)//g<left><left>

" custom git commands
nnoremap gA :!~/Scripts/commit<Enter>
nnoremap ga :!git<space>add<space>.<Enter><Esc>
nnoremap gc :!git<space>commit<space><Enter><Esc>
nnoremap gp :!git<space>push<space><Enter>
nnoremap gP :!git<space>pull<space><Enter>

" dev binds
map <F8> :call CloseNerdtreeTagbar()<CR><Enter>
map <F2> :call ToggleNerdtreeTagbar()<CR><Enter>


" general binds
map <c-t> :tabe<CR>
nmap <Bar> :vsplit<space>
nmap - :split<space>
nmap <C-J> <C-W><C-J>
nmap <C-K> <C-W><C-K>
nmap <C-L> <C-W><C-L>
nmap <C-H> <C-W><C-H>


" build tags from own project
map <F7> :!ctags -R --sort=yes --c++-kinds=+p --fields=+iaS --extra=+q .<CR><Enter>
